package shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sale {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("employee_id")
    @Expose
    private Integer employeeId;
    @SerializedName("carmodel_id")
    @Expose
    private Integer carmodelId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getCarmodelId() {
        return carmodelId;
    }

    public void setCarmodelId(Integer carmodelId) {
        this.carmodelId = carmodelId;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "id=" + id +
                ", employeeId=" + employeeId +
                ", carmodelId=" + carmodelId +
                '}';
    }
}