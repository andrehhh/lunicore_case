package shop;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Database {
    @SerializedName("carshop")
    private Carshop carshop;

    public boolean addCarmodel(Carmodel cm) {
        return getCarshop().getCarmodels().add(cm);
    }

    public Optional<Employee> getEmployee(int id) {
        Employee emp = (Employee)carshop.getEmployees().stream()
                .filter(employee -> employee.getId() == id)
                .toArray()[0]; //TODO fix hack.
        return Optional.ofNullable(emp);

    }

    public int getTotalSales(Employee employee) {
        return getSaleSum(getSales(employee));
    }

    public List<Sale> getSales(Employee employee) {
        List<Sale> sales = carshop.getSales();
        List<Sale> result = new LinkedList<>();

        for(Sale s : sales) {
            if(s.getEmployeeId() == employee.getId()) {
                result.add(s);
            }
        }
        return result;
    }

    public Optional<Carmodel> getCarmodelById(int carId) {
        List<Carmodel> carmodelList = carshop.getCarmodels();
        Carmodel result = null;
        for(Carmodel cm : carmodelList) {
            if(cm.getId() == carId) {
                result = cm;
            }
        }
        return Optional.of(result);
    }

    public int getSaleSum(List<Sale> sales) {
        int sum = 0;
        for(Sale s : sales) {
            Optional<Carmodel> cm = getCarmodelById(s.getCarmodelId());
            if(cm.isPresent()) {
                sum += cm.get().getPrice();
            }
        }
        return sum;
    }

    public Carshop getCarshop() {
        return carshop;
    }

    public void setCarshop(Carshop carshop) {
        this.carshop = carshop;
    }
}