package shop;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Carshop {

    @SerializedName("employees")
    @Expose
    private List<Employee> employees = null;
    @SerializedName("carmodels")
    @Expose
    private List<Carmodel> carmodels = null;
    @SerializedName("sales")
    @Expose
    private List<Sale> sales = null;

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Carmodel> getCarmodels() {
        return carmodels;
    }

    public void setCarmodels(List<Carmodel> carmodels) {
        this.carmodels = carmodels;
    }

    public List<Sale> getSales() {
        return sales;
    }

    public void setSales(List<Sale> sales) {
        this.sales = sales;
    }

}
