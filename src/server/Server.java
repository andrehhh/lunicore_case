package server;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.Optional;
import java.util.Scanner;

import com.google.gson.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import shop.*;


public class Server {
    private HttpServer server;
    private String DBPath;
    private Database db;
    private GsonBuilder gsonBuilder;


    public Server(int port, String DBPath) {

        this.DBPath = DBPath;

        gsonBuilder = new GsonBuilder().setPrettyPrinting();
        try {
            loadDB(DBPath);
            initializeServer(8000);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void initializeServer(int port) throws IOException {
        server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/employees", ( ex -> employeeHandler(ex)));
        server.createContext("/carmodels", ( ex -> carmodelsHandler(ex)));
        server.createContext("/total_sales", ( ex -> totalSalesHandler(ex)));
        server.setExecutor(null);
        server.start();
        System.out.println("HttpServer started");
    }



    private void loadDB(String path) throws IOException {
        Gson gson = gsonBuilder.create();
        db = gson.fromJson(readFile(path), Database.class);
        if(db == null) {
            throw new IOException("Failed to load Json: "+path);
        } else {
            System.out.println("Database successfully loaded: " +db.getCarshop().getCarmodels().size()+"," +
                    db.getCarshop().getEmployees().size() +","+
                    db.getCarshop().getSales().size());
        }
    }



    private void employeeHandler(HttpExchange ex) throws IOException {
        if( ex.getRequestMethod().equals("GET") ) {
            System.out.println("GET-request on employees from "+ex.getRemoteAddress());
            Gson gson = gsonBuilder.create();
            String response = gson.toJson(db.getCarshop().getEmployees());
            ex.sendResponseHeaders(200, 0);
            OutputStream os = ex.getResponseBody();
            os.write(response.getBytes("ISO-8859-1"));
            os.close();
        } else {
            ex.sendResponseHeaders(405, -1);
        }
    }

    private void carmodelsHandler(HttpExchange ex) throws IOException {
        if( ex.getRequestMethod().equals("GET")) {
            System.out.println("GET-request on carmodels from "+ex.getRemoteAddress());
            Gson gson = gsonBuilder.create();
            String response = gson.toJson(db.getCarshop().getCarmodels());
            ex.sendResponseHeaders(200, 0);
            OutputStream os = ex.getResponseBody();
            os.write(response.getBytes("ISO-8859-1"));
            os.close();
        } else if( ex.getRequestMethod().equals("POST")) {
            System.out.println("POST-request on carmodels from "+ex.getRemoteAddress());
            Gson gson = gsonBuilder.create();
            InputStream requestBody = ex.getRequestBody();

            Scanner s = new Scanner(requestBody).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";

            boolean success = db.addCarmodel(gson.fromJson(result, Carmodel.class));
            if(success) {
                System.out.println("\tCarmodel OK!");
                ex.sendResponseHeaders(200, 0);
                OutputStream os = ex.getResponseBody();
                os.write(result.getBytes("ISO-8859-1"));
                ex.close();
                System.out.print("Save to file disabled.");
            } else {
                ex.sendResponseHeaders(400, -1);
            }

        } else {
            ex.sendResponseHeaders(405, -1);
        }
    }

    private void totalSalesHandler(HttpExchange ex) throws IOException {
        if( ex.getRequestMethod().equals("GET") ) {
            System.out.println("GET-request on total sales from "+ex.getRemoteAddress());
            Gson gson = gsonBuilder.create();
            String employees = gson.toJson(db.getCarshop().getEmployees());
            JsonParser jp = new JsonParser();
            JsonArray ja = jp.parse(employees).getAsJsonArray();
            for(JsonElement je : ja) {
                JsonObject employee = je.getAsJsonObject();

                int employeeID = gson.fromJson(employee.get("id"), Integer.class);

                Optional<Employee> emp = db.getEmployee(employeeID);
                if(emp.isPresent()) {
                    int totalSales = db.getTotalSales(emp.get());
                    employee.add("total_sales", new JsonPrimitive(totalSales));
                } else {
                    employee.add("total_sales", new JsonPrimitive(0));
                }
            }
            String finishedJson = gson.toJson(ja);
            ex.sendResponseHeaders(200, 0);
            OutputStream os = ex.getResponseBody();
            os.write(finishedJson.getBytes("ISO-8859-1"));
            os.close();
            ex.close();
        }
    }

    private String readFile(String fileName) throws IOException {
        FileReader fr = new FileReader(fileName);
        StringBuilder sb = new StringBuilder();

        int current = fr.read();

        while(current != -1) {
            sb.append((char)current);
            current = fr.read();
        }
        return sb.toString();
    }

    private void saveToFile(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        System.out.print("Starting file-save...");
        PrintWriter pw = new PrintWriter(fileName, "ISO-8859-1");
        Gson gson = gsonBuilder.create();
        String carshopJson = gson.toJson(db.getCarshop());
        pw.print(carshopJson);
        pw.close();
        System.out.println("... file-save OK!");
    }
}
